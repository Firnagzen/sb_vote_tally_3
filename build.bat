@ECHO OFF

set kivy_portable_root=%~dp0..\SublimeGitKivy\kivy\

set GST_REGISTRY=%kivy_portable_root%gstreamer\registry.bin
ECHO GST_REGISTRY
ECHO %GST_REGISTRY%
ECHO ---------------

set GST_PLUGIN_PATH=%kivy_portable_root%gstreamer\lib\gstreamer-0.10
ECHO GST_PLUGIN_PATH:
ECHO %GST_PLUGIN_PATH%
ECHO ---------------

set PATH=%kivy_portable_root%;%kivy_portable_root%Python;%kivy_portable_root%Python\Scripts;%kivy_portable_root%gstreamer\bin;%kivy_portable_root%MinGW\bin;%PATH%
ECHO PATH:
ECHO %PATH%
ECHO ----------------------------------

set PYTHONPATH=%kivy_portable_root%kivy;%PYTHONPATH%
ECHO PYTHONPATH:
ECHO %PYTHONPATH%
ECHO ----------------------------------

set LOC=%~dp0

Setlocal EnableDelayedExpansion

FOR /f "tokens=2 delims='" %%G IN ('FIND "__version__ = " main.py') DO SET VERSION=%%G

rm dist\SBVoteTally-3*.exe

python "%kivy_portable_root%Python\Scripts\pyinstaller-script.py" "SB Vote Tally Kivy.spec"

start "" "dist\SBVoteTally-%VERSION%.exe"

pause