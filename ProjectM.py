import re
import htmlentitydefs
from io import BytesIO
import lxml.etree as etree
from itertools import ifilter


class ordered_dict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self._order = self.keys()

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key in self._order:
            self._order.remove(key)
        self._order.append(key)

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        self._order.remove(key)

    def order(self):
        return self._order[:]

    def ordered_items(self):
        return [(key,self[key]) for key in self._order]


class Vote(object):
    """
    Vote takes either a HTML tree (expected to start at li) or another Vote 
    object via the .from_Vote and .from_Node methods.
    
    Usable attributes are .posters and .text 
    .posters returns a dictionary of usernames to post indices
    .text returns a list of strings of the voted text
    
    Usable methods are break_page(page), .merge(incoming_Vote_object), 
    .stripped_username(), and .stripped_text()
    """
    
    post_xpath = etree.XPath(".//li[@data-author and not(contains(.,'#####'))]")
                                 
    pre_vote_xpath = etree.XPath("./*[contains(.,'[x]') or contains(.,'[X]')]")
                                     
    op_xpath = etree.XPath(".//div[@class='titleBar']//a[@class='username']")
    
    end_xpath = etree.XPath(".//div[@class='PageNav']")
    
    magical_parser = etree.XMLParser(encoding='utf-8', recover=True)
    
    vote_re = re.compile("(?:-|<[^/>]*>)* *\[(?:<[^>]*>)?[Xx](?:<[^>]*>)?\].+$", 
                         flags=re.MULTILINE|re.UNICODE)
                         
    vote_xpath = etree.XPath(".//blockquote[@class='messageText SelectQuoteContainer ugc baseHtml']")
                    
    post_number_xpath = etree.XPath(".//a[@class='item muted postNumber"
                                    " hashPermalink OverlayTrigger']/text()")
                                    
    post_index_xpath = etree.XPath(".//a[@title='Permalink']")
   
    quote_block_xpath = etree.XPath(".//div[@class='bbCodeBlock "
                                    "bbCodeQuote']/aside")
    
    quoted_content_xpath = etree.XPath("./blockquote//text()")
    
    vote_stripper_re = re.compile("<[^>]*>|\[[^]]\]|[^\w\d]|[Pp]lan")
    
    state_re = re.compile("^(this.{0,2}|support(ing this)?.{0,2})$", 
                          flags=re.MULTILINE)
                      
    cleanup_re = '<(color|font|size|URL)[^=]*=[\"\']*([^>\"\']*)[\"\']*>'
        
    test_xpath = etree.XPath(".//span[contains(@style, 'size') or "
                                     "contains(@style, 'color') or "
                                     "contains(@style, 'font') or "
                                     "contains(@style, 'text-decoration')]|"
                             ".//div[contains(@style, 'left') or"
                                    "contains(@style, 'right') or"
                                    "contains(@style, 'center')]|"
                             ".//a[@class='externalLink']|"
                             ".//br")
                             
    text_deco_re = re.compile("(color|size|font)(?:-family)?: (.*)")
    
    struck_through_xpath = etree.XPath("self::span[(contains(.,'[x]') "
                                       "or contains(.,'[X]'))]")

    table_re = re.compile("<table.*</table>")

    
    def __init__(self, posters, vote_text, post_number):
        self.posters = posters
        self.post_numbers = post_number
        self.text = [i for i in vote_text]
    
    @classmethod
    def from_Vote(cls, vote_object, text):
        """Expects 'text' as a single string."""
        cls.inc_posters = ordered_dict(vote_object.posters.ordered_items())
        cls.inc_posters._order = vote_object.posters._order[:]
        cls.inc_text = [text]
        cls.inc_post_numbers = vote_object.post_numbers
        return cls(cls.inc_posters, cls.inc_text, cls.inc_post_numbers)
    
    @classmethod
    def from_Node(cls, node):
        # print(cls._stringify(node))
        # print('###########################')
        cls.inc_text = cls._init_state(node)
        cls.inc_posters = ordered_dict()
        cls.inc_posters[node.attrib["data-author"]] = cls.post_index(node)
        cls.inc_post_number = int(cls.post_number_xpath(node)[0][1:])
        return cls(cls.inc_posters, cls.inc_text, cls.inc_post_number)
        
    @classmethod
    def post_index(cls, node):
        href = cls.post_index_xpath(node)[0].get("href")
        return re.sub("[^#]*#post-", "", href)
    
    @classmethod
    def _init_state(cls, raw):
        cls.inc_text, cls.quote = cls._extract_votes(raw, cls.vote_xpath)
        cls.first_vote = re.sub(cls.vote_stripper_re,"",cls.inc_text[0]).lower()
        if cls.state_re.findall(cls.first_vote):
            for i in cls.quote:
                cls.joined_quote = ''.join(cls.quoted_content_xpath(i))
            try:
                cls.quoted_text = cls.vote_re.findall(cls.joined_quote)
            except AttributeError:
                pass
            else:
                if cls.quoted_text:
                    cls.inc_text = cls.quoted_text
        return cls.inc_text
    
    @classmethod
    def _extract_votes(cls, raw, xp):
        cls._clean_text(raw)
        cls.quote = cls.quote_block_xpath(raw)
        for i in cls.quote:
            try:
                i.getparent().remove(i)
            except:
                pass
        cls.excised = xp(raw)
        cls.joined = "".join(cls._stringify(i) for i in cls.excised)
        cls.extracted = cls.vote_re.findall(cls.joined)
        if not cls.extracted:
            raise ValueError
        return cls.extracted, cls.quote
        
    @classmethod
    def span_deco(cls, elem):
        cls.style = elem.attrib["style"]
        try:
            cls.dispatch[cls.style](elem)
        except KeyError:
            cls.deco_type = cls.text_deco_re.findall(cls.style)
            elem.tag = cls.deco_type[0][0]
            try:
                elem.attrib["style"] = cls.dispatch[cls.deco_type[0][1]]
            except KeyError:
                elem.attrib["style"] = cls.deco_type[0][1]
        
    @classmethod
    def span_underline(cls, elem):
        elem.tag = "u"
        elem.attrib.pop("style")
        
    @classmethod
    def span_strikethrough(cls, elem):
        if cls.struck_through_xpath(elem):
            elem.getparent().remove(elem)
        else:
            elem.tag = "s"
            elem.attrib.pop("style")
        
    @classmethod
    def div(cls, elem):
        elem.tag = "remove"
        try:
            elem.tail = "\n" + elem.tail
        except TypeError:
            elem.tail = "\n"
        
    @classmethod
    def a(cls, elem):
        elem.tag = "URL"
        for i in elem.attrib:
            if i != "href":
                elem.attrib.pop(i)
        
    @classmethod
    def br(cls, elem):
        try:
            elem.tail = "\n" + elem.tail
        except TypeError:
            elem.tail = "\n"
        
    @classmethod
    def _clean_text(cls, raw):
        try:
            cls.dispatch
        except AttributeError:
            cls.dispatch = {
                            "span" : cls.span_deco,
                            "text-decoration: underline" : cls.span_underline,
                            "text-decoration: "
                            "line-through" :cls.span_strikethrough,
                            "div" : cls.div,
                            "a" : cls.a,
                            "br" : cls.br,
                            "tiny":"1", "small":"3", "medium":"5", "large":"10"
                            }
        for elem in cls.test_xpath(raw):
            try:
                cls.dispatch[elem.tag](elem)
            except KeyError:
                print "whut?"
        etree.strip_elements(raw, 'img', with_tail = False)
        etree.strip_elements(raw, 'br', with_tail = False)
        etree.strip_tags(raw, 'remove')
        
    @classmethod
    def _stringify(cls, item):
        try:
            cls.stringified = etree.tostring(item)
        except TypeError:
            cls.stringified = item
        cls.stringified = re.sub(" +\[", "[", cls.stringified)
        return re.sub(cls.cleanup_re, "<\\1='\\2'>", cls.stringified)

    @classmethod
    def fuck_tables(cls, text):
        return cls.table_re.sub("", text)
        
    @classmethod
    def break_page(cls, page):
        if not page:
            raise ValueError
        cls.vote_list = []
        page = cls.fuck_tables(page)
        cls.root = etree.parse(BytesIO(page), cls.magical_parser).getroot()
        cls.op = cls.op_xpath(cls.root)[0].text
        cls.op = re.sub("[^\w\d\.]", "", cls.op).lower()
        try:
            cls.end_page = int(cls.end_xpath(cls.root)[0].get("data-last"))
        except IndexError:
            cls.end_page = 1
        cls.post_list = ifilter(cls.pre_vote_xpath, cls.post_xpath(cls.root))
        cls.add_to_vote_list = cls.vote_list.append
        for indv_post in cls.post_list:
            try:
                cls.add_to_vote_list(Vote.from_Node(indv_post))
            except (ValueError, IndexError):
                pass
        return cls.vote_list, cls.op, cls.end_page
        

    def stripped_text(self):
        try:
            return self.stripped
        except AttributeError:
            self.stripped = "".join(
                                    re.sub(self.vote_stripper_re, "", line
                                    ).lower() for line in self.text)
        return self.stripped
        
    def stripped_username(self):
        return re.sub("[^\w\d\.]", "", self.posters._order[0]).lower()
        
    def merge(self, inc_vote):
        # post_numbers is not retained in merged votes; it should never
        # be used subsequent to merging.
        self.posters.update(inc_vote.posters)
        # print "MERGE", inc_vote.posters._order[0]
        self.posters._order += inc_vote.posters._order
        
        
        
def check_URL(URL):
    URL_re = re.compile("(?:http://forums.sufficientvelocity.com/threads/)?"
                        "([^:/.]*\.[0-9]+)/?(?:page-[0-9]*)?")
    try:
        return URL_re.findall(URL)[0]
    except IndexError:
        return ""
    
    
def cull(vote_list, op, start = 0, end = 0):
    if not vote_list:
        raise ValueError
    seen = ordered_dict()
    if start:
        post_number_range = xrange(start, end)
    for i in vote_list:
        cur_username = i.stripped_username()
        cur_stripped = i.stripped_text()
        if start:
            if i.post_numbers in post_number_range and op != cur_username:
                seen[cur_username] = i
        else:
            if op != cur_username:
                seen[cur_username] = i
    return [seen[i] for i in seen.order()]
    
    
def break_votes(vote_list):
    if not vote_list:
        raise ValueError
    return [Vote.from_Vote(i, re.sub("<[^>]*>", "", j))
            for i in vote_list for j in i.text]
    
    
def group_votes(vote_list):
    if not vote_list:
        raise ValueError
    groupedu = ordered_dict()
    groupedt = ordered_dict()
    for i in vote_list:
        text = i.stripped_text()
        unam = i.stripped_username()
        try:
            [j.merge(i) for j in groupedu[text]]
        except KeyError:
            try:
                groupedt[text].merge(i)
                try:
                    groupedu[unam].append(groupedt[text])
                except KeyError:
                    groupedu[unam] = [groupedt[text]]
                continue
            except KeyError:
                groupedt[text] = i
        try:
            groupedu[unam].append(i)
        except KeyError:
            groupedu[unam] = [i]
    return [groupedt[i] for i in groupedt.order()]
    
    
def unescape(text):
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)
    
    
def consolidate(vote_list, multiline=True, version=""):
    if multiline:
        return unescape((
            "[b]Vote tally:[/b]\n[color=transparent]##### " +
            version + "[/color]\n" +
            "\n\n".join(
                        (
                         "\n".join(
                                   re.sub("<([^>]*)>", "[\\1]", line) 
                                   for line in vote.text
                                   ) + 
                         "\n[b]No. of votes: " + str(len(vote.posters)) +
                         "[/b]\n" +
                         "\n".join(
                               "[url=http://forums.sufficientvelocity.com/"
                               + number + "]" + voter + "[/url]" 
                               for voter, number in vote.posters.ordered_items()
                                   )
                        )for vote in vote_list
                       )
                ))
    else:
        return unescape((
            "[b]Vote tally:[/b]\n[color=transparent]##### " +
            version + "[/color]\n" +
            "\n\n".join(
                        (
                         "\n".join(
                                   re.sub("<([^>]*)>", "[\\1]", line) 
                                   for line in vote.text
                                   ) + 
                         "\n[b]No. of votes: " + str(len(vote.posters)) +
                         "[/b]\n" +
                         ", ".join(
                            "[url=http://forums.sufficientvelocity.com/"
                            + number + "]" + voter + "[/url]" 
                            for voter, number in vote.posters.ordered_items()
                                )
                        )for vote in vote_list
                       )
                ))
    
    
    
if __name__ == "__main__":
    print("Buh?")
    # import subprocess
    # subprocess.call("cls", shell=True)
    # import requests
    # URL = 'http://forums.sufficientvelocity.com/threads/hsdk-a-meeting-of-master-and-student.256223/page-28'
    # page = requests.get(URL).content
    # page_list = []
    # for i in xrange(1, 54):
        # page_list.append(requests.get(URL + str(i)))
        # print str(i) + "loaded"
    # import cPickle as pickle
    # with open("downloaded_pages.txt", "wb") as f:
        # pickle.dump(page_list, f)
    # with open("downloaded_pages.txt", "rb") as f:
        # page_list = pickle.load(f)
    with open("asdf.txt", "r") as f:
        page = f.read()
    # import cProfile
    # def break_pages(page_list):
        # vote_list_final = []
        # for p in page_list:
            # vote_list, op, end = Vote.break_page(p.content)
            # vote_list_final += vote_list
    # cProfile.run("break_pages(page_list)", "profile")
    vote_list, op, end = Vote.break_page(page)
    vote_list = cull(vote_list, op)
    # vote_list = break_votes(vote_list)
    vote_list = group_votes(vote_list)
    print(consolidate(vote_list).encode('utf-8'))
    
