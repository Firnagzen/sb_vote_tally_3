# -*- mode: python -*-
from kivy.tools.packaging.pyinstaller_hooks import install_hooks
install_hooks(globals())

mainfile = 'main.py'

with open("main.py", "r") as f:
    for line in f:
        if '__version__ =' in line:
            version = line.split("'")[1]
            break

a = Analysis([mainfile],
             hiddenimports=[],
             runtime_hooks=None)
             
pyz = PYZ(a.pure)

for d in a.datas:
    if 'pyconfig' in d[0]: 
        a.datas.remove(d)
        break
        
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas + [('VoteTally.kv', '.\\VoteTally.kv', 'DATA')],
          name='SVVoteTally-' + version + '.exe',
          debug=False,
          console=False,
          strip=None,
          upx=True, icon='x.ico')
