import os, PyInstaller.main

cur_dir = os.getcwd()

for i in os.listdir(cur_dir):
    if i == "main.py":
        kivy = True
        with open("main.py", "r") as f:
            for j in f.readlines():
                if j.startswith("__version__"):
                    version = j[15:-2]
                    break
        break
    elif i == "tkinterGUI.py":
        kivy = False
        with open("tkinterGUI.py", "r") as f:
            for j in f.readlines():
                if j.startswith("__version__"):
                    version = j[15:-2]
                    break
        break

for i in os.listdir(cur_dir + os.sep + "dist"):
    if kivy:
        if i.startswith("SVVoteTally-") and i.endswith(".exe"):
            os.remove(cur_dir + os.sep + "dist" + os.sep + i)
            break
    else:
        if i.startswith("SVVoteTallytk-"):
            os.remove(cur_dir + os.sep + "dist" + os.sep + i)
            break
if kivy:
    PyInstaller.main.run(pyi_args=["SB Vote Tally Kivy.spec"])
    os.startfile(cur_dir+os.sep+"dist"+os.sep+"SVVoteTally-"+version+".exe")
else:
    PyInstaller.main.run(pyi_args=["SB Vote Tally tkinter.spec"])
    os.startfile(cur_dir+os.sep+"dist"+os.sep+"SVVoteTallytk-"+version+".exe")
