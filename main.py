import math
import threading
from time import time
from ProjectM import *
import cPickle as pickle
from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.boxlayout import BoxLayout
from kivy.core.clipboard import Clipboard
from kivy.network.urlrequest import UrlRequest
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.properties import StringProperty, NumericProperty, ObjectProperty


__version__ = '3.21'

    
class CustomTextInput(BoxLayout):
    input_URL = StringProperty('Input new URL here!')
    def call_dropdown(self):
        pass
        
    def get_from_clipboard(self, *args):
        pass
        
        
class CustomButton(Button):
    def dropdown_select(self, button):
        pass
        
        
class CustomDropDown(DropDown):
    def toggle_votes(self):
        self.ids.individual_votes.active = not self.ids.individual_votes.active
        self.dismiss()
        
    def toggle_post(self):
        self.ids.by_post_numbers.active = not self.ids.by_post_numbers.active
        self.dismiss()
        
    def toggle_multiline(self):
        self.ids.multiline_output.active = not self.ids.multiline_output.active
        self.dismiss()
        
    def toggle_delete(self):
        self.ids.delete_mode.active = not self.ids.delete_mode.active
        self.dismiss()
        
    def dump_cache(self):
        pass

        
class E(ExceptionHandler):
    def handle_exception(self, inst):
        import traceback
        main_widget.output_text=("An unexpected error has occured! No, "
                                 "seriously. You shouldn't be seeing this. An "
                                 "error log has been created in " +
                                 main_widget.storage + "; please send it to me"
                                 " on the forums.\n~Firnagzen"
                                 )
        with open(main_widget.storage + "/ERROR_LOG.txt", "w") as f:
            f.write(
                traceback.format_exc() + "\r\n" +
                main_widget.URL + "\r\n"
                "Start, end, current pages: " + str(main_widget.start) + 
                ", " + str(main_widget.end) + ", " + 
                str(main_widget.cur_page) + "\r\n" +
                str(main_widget.options_dropdown.ids.multiline_output.active) +
                str(main_widget.options_dropdown.ids.by_post_numbers.active) +
                str(main_widget.options_dropdown.ids.individual_votes.active)
                    )
        return ExceptionManager.PASS
        
    
class FirstWidget(Widget):
    max_loading = NumericProperty(1)
    output_text = StringProperty("")
    start_page = StringProperty(1)
    end_page = StringProperty(0)
    loading = NumericProperty(0)
    load_state = 0
    
    def __init__(self, storage, **kwargs):
        super(FirstWidget, self).__init__(**kwargs)
        self.storage = storage
        self._init_store()
        self._init_input()
        self._init_options()
    
    def _init_store(self):
        try:
            with open(self.storage + "/stored_pages.dat", "rb") as f:
                self.store = pickle.load(f)
        except (IOError, EOFError):
            self.store = dict()
        try:
            with open(self.storage + "/past_URLs.dat", "rb") as f:
                self.past_URLs = pickle.load(f)
        except (IOError, EOFError):
            self.past_URLs = dict()
            
    def _init_input(self):
        if self.past_URLs:
            self.input_state = 0
            self._init_dropdown()
            self.ids.input_box.add_widget(self.mainbutton)
        else:
            self.input_state = 1
            self._init_input_textbox()
            self.ids.input_box.add_widget(self.text_input)
            
    def _init_input_textbox(self):
        self.text_input = CustomTextInput()
        self.text_input.call_dropdown = self.call_dropdown
        self.text_input.get_from_clipboard = self.get_from_clipboard
            
    def _init_dropdown(self):
        self.dropdown = DropDown()
        for i in sorted(self.past_URLs.iterkeys(), 
                        key=lambda j: j.decode('utf-8')):
            if not i[-1:].isdigit():
                continue
            btn = CustomButton(text = i)
            btn.dropdown_select = self.dropdown_select
            self.dropdown.add_widget(btn)
            try:
                self.buffer_URL
            except AttributeError:
                self.buffer_URL = i
        btn = CustomButton(text = "New entry")
        btn.bind(on_release = self.switch_to_input)
        self.dropdown.add_widget(btn)
        try:
            self.first_URL = self.store["recent_URL"]
            self.past_URLs[self.first_URL]
        except KeyError:
            self.first_URL = self.buffer_URL
            # self.first_URL = sorted(self.past_URLs.iterkeys())[0]
        self.mainbutton = CustomButton(text = self.first_URL)
        self.mainbutton.bind(on_release = self.dropdown.open)
        self.input_URL = self.first_URL
        self.dropdown.bind(on_select=lambda instance, x: 
                           setattr(self.mainbutton, 'text', x))
            
    def dropdown_select(self, button):
        if not self.options_dropdown.ids.delete_mode.active:
            self.input_URL = button.text
            self.dropdown.select(button.text)
        else:
            self.dropdown.remove_widget(button)
            del self.past_URLs[button.text]
            self.dropdown.select(sorted(i for i in self.past_URLs.iterkeys() 
                                        if i[-1:].isdigit())[0])
            self.input_URL = self.mainbutton.text
            with open(self.storage + "/past_URLs.dat", "wb") as f:
                pickle.dump(self.past_URLs, f)
        
    def switch_to_input(self, *args):
        self.input_state = 1
        self._init_input_textbox()
        self.dropdown.dismiss()
        self.ids.input_box.remove_widget(self.mainbutton)
        self.ids.input_box.add_widget(self.text_input)
        
    def switch_to_dropdown(self):
        self.input_state = 0
        self._init_dropdown()
        self.ids.input_box.remove_widget(self.text_input)
        self.ids.input_box.add_widget(self.mainbutton)
        self.dropdown.open(self.mainbutton)
        
    def call_dropdown(self):
        if self.past_URLs:
            self.switch_to_dropdown()
        else:
            pass

    def get_from_clipboard(self, *args):
        for i in ["TEXT", "UTF8_STRING", "STRING", "text/plain;charset=utf-8"]:
            try:
                self.text_input.input_URL = Clipboard.get(i)
                break
            except:
                pass
            
    def _init_options(self):
        self.options_dropdown = CustomDropDown()
        try:
            self.options_dropdown.ids.individual_votes.active = (
                                             self.past_URLs["individual_votes"])
            self.options_dropdown.ids.by_post_numbers.active = (
                                             self.past_URLs["by_post_numbers"])
            self.options_dropdown.ids.multiline_output.active = (
                                             self.past_URLs["multiline_output"])
        except KeyError:
            pass
        self.options_dropdown.ids.delete_mode.bind(active=self.delete_changed)
        self.options_mainbutton = Button(text='Options', size_hint_y=None,
                                         height='26dp', font_size='14dp')
        self.options_mainbutton.bind(on_release = self.options_dropdown.open)
        self.ids.options_box.add_widget(self.options_mainbutton)
        self.options_dropdown.dump_cache = self.dump_cache
        
    def delete_changed(self, *args):
        if self.options_dropdown.ids.delete_mode.active:
            self.output_text += ("Delete mode is active! Any URLs selected will"
                                 " be deleted.\n")
        else:
            self.output_text += "Delete mode deactivated.\n"
        
    def dump_cache(self):
        self.output_text = "Cached pages cleared!"
        self.options_dropdown.dismiss()
        self.store["recent_pages"] = dict()
        self.store["recent_page_listing"] = []
        with open(self.storage + "/stored_pages.dat", "wb") as f:
            pickle.dump(self.store, f)
        
    def validate_input(self):
        self.ids.output_box.focus = False
        self.ids.n_in_start.focus = False
        self.ids.n_in_end.focus = False
        self.output_text = ""
        self.imn = 0
        if self.input_state:
            self.input_URL = self.text_input.input_URL
        self.URL = check_URL(self.input_URL)
        if self.URL:
            self.check_URL_store()
            self.check_numbers()
        else:
            self.output_text = "Invalid URL!"
        
    def check_URL_store(self):
        self.cur_time = time()
        if self.URL in self.past_URLs:
            if self.cur_time - self.past_URLs[self.URL] < 2592000:
                self.past_URLs[self.URL] = time()
            else:
                del self.past_URLs[self.URL]
        else:
            self.past_URLs[self.URL] = time()
        self.past_URLs["individual_votes"] = (
                              self.options_dropdown.ids.individual_votes.active)
        self.past_URLs["by_post_numbers"] = (
                               self.options_dropdown.ids.by_post_numbers.active)
        self.past_URLs["multiline_output"] = (
                              self.options_dropdown.ids.multiline_output.active)
        with open(self.storage + "/past_URLs.dat", "wb") as f:
            pickle.dump(self.past_URLs, f)
            
    def check_numbers(self):
        try:
            int(self.start_page)
        except ValueError:
            self.output_text = "Start point must be a number!"
        else:
            if not self.end_page:
                self.output_text = ("End point not detected! Will tally until" +
                                    " end of thread...\n")
                self.end_page = '120000'
            try:
                int(self.end_page)
            except ValueError:
                self.output_text = (self.output_text + 
                                    "End point must be a number!")
            else:
                if int(self.start_page) <= int(self.end_page):
                    self.start_post = 0
                    self.end_post = 0
                    if self.options_dropdown.ids.by_post_numbers.active:
                        self.start_post = int(self.start_page)
                        self.end_post = int(self.end_page)
                        self.start = int(math.ceil(float(self.start_page)/25))
                        self.end = int(math.ceil(float(self.end_page)/25))
                    else:
                        self.start = int(self.start_page)
                        self.end = int(self.end_page)
                    self.init_load()
                else:
                    self.output_text = "End number must be larger than start!"
            
    def init_load(self):
        self.imn = 0
        self.cur_time = 0
        self.loaded_pages = []
        self.new_pages = dict()
        self.new_page_listing = []
        self.cur_page = self.start
        try:
            self.load_state
        except AttributeError:
            self.load_state = 0
        try:
            with open(self.storage + "/stored_pages.dat", "rb") as f:
                self.store = pickle.load(f)
        except (IOError, EOFError):
            self.store = dict()
        if "recent_pages" not in self.store:
            self.store["recent_pages"] = dict()
        if "recent_URL" not in self.store:
            self.store["recent_URL"] = ""
        if "recent_page_listing" not in self.store:
            self.store["recent_page_listing"] = []
        if self.URL == self.store["recent_URL"]:
            self.cur_time = time()
        self.load_decision()
            
    def load_decision(self):
        self.l_count = 0
        if self.cur_page <= self.end:
            if self.cur_time:
                if not self.load_state:
                    if self.cur_page in self.store["recent_page_listing"][:-1]:
                        if ((self.cur_time -
                             self.store["recent_pages"][self.cur_page][0])
                            < 900):
                            self.load_store()
                        else:
                            self.load_page()
                    else:
                        self.load_page()
                else:
                    if self.cur_page in self.store["recent_page_listing"]:
                        if ((self.cur_time -
                             self.store["recent_pages"][self.cur_page][0])
                            < 900):
                            self.load_store()
                        else:
                            self.load_page()
                    else:
                        self.load_page()
            else:
                self.load_page()
        else:
            self.load_completed()
    
    def load_store(self):
        self.loaded_pages += self.store["recent_pages"][self.cur_page][1]
        self.output_text += "Page "+str(self.cur_page)+" loaded from memory!\n"
        self.new_pages[self.cur_page] =self.store["recent_pages"][self.cur_page]
        self.new_page_listing.append(self.cur_page)
        self.cur_page += 1
        self.load_decision()
    
    def load_page(self, *args):
        self.loading = 0
        self.cur_URL = ("http://forums.sufficientvelocity.com/threads/" + 
                        self.URL +"/")
        if self.cur_page > 1:
            self.cur_URL = self.cur_URL + "page-" + str(self.cur_page)
        self.output_text += "Loading page " + self.cur_URL + "\n"
        x = UrlRequest(self.cur_URL, 
                       on_success = self.load_success, 
                       on_error = self.load_error, 
                       on_progress = self.load_progress,
                       chunk_size = 1024)
    
    
    def load_success(self, req, result):
        try:
            if (self.end != 120000) and (self.end != 4800):
                self.broken_page, self.op, _ = Vote.break_page(result)
            else:
                self.broken_page, self.op, self.end = Vote.break_page(result)
        except TypeError:
            self.load_error(None, "Empty page loaded!\n")
        except ValueError:
            pass
        else:
            self.output_text += "Page " + str(self.cur_page) + " loaded!\n"
            self.new_pages[self.cur_page] = (time(), self.broken_page)
            self.new_page_listing.append(self.cur_page)
            self.loaded_pages += self.broken_page
            self.cur_page += 1
            self.load_decision()
    
    def load_error(self, req, error):
        self.l_count += 1
        self.output_text += str(error) + "\n"
        if self.l_count <= 3:
            if error == "Empty page loaded!":
                self.output_text+="Sleeping for 30 seconds before retrying...\n"
                Clock.schedule_once(self.load_page, 30)
            else:
                self.output_text += "Retrying!\n"
                self.load_page()
        else:
            self.output_text +="Failed to load all pages! Please try again...\n"
            self.load_state = 1
            self.load_dump()
    
    def load_progress(self, req, current_size, total_size):
        if total_size:
            self.loading = current_size
            self.max_loading = total_size
        else:
            if not self.imn:
                self.output_text += self.cur_URL + " is not a valid page!\n"
                self.load_completed()
            self.imn += 1
            
    def load_completed(self):
        self.load_state = 0
        if self.loaded_pages:
            self.load_dump()
            t = threading.Thread(target=self.tally, 
                                 args=(self.loaded_pages, ))
            t.start()
        else:
            self.output_text += ("No votes were found! Are the URL, start and "+
                                 "end points correct?")
        
        
    def load_dump(self):
        self.store["recent_page_listing"] = self.new_page_listing
        self.store["recent_URL"] = self.URL
        self.store["recent_pages"] = self.new_pages
        with open(self.storage + "/stored_pages.dat", "wb") as f:
            pickle.dump(self.store, f)
        
    def tally(self, page_list):
        self.output_text = "Consolidating votes..."
        try:
            self.vote_list = cull(page_list, self.op, self.start_post, 
                                  self.end_post)
            if self.options_dropdown.ids.individual_votes.active:
                self.vote_list = break_votes(self.vote_list)
            self.vote_list = group_votes(self.vote_list)
        except (ValueError, AttributeError):
            self.output_text = (self.output_text + "No votes were loaded. Are "+
                                "you sure the URL and numbers are correct?\n")
        else:
            self.buffer = consolidate(self.vote_list, 
                              self.options_dropdown.ids.multiline_output.active,
                              version = __version__)
            Clock.schedule_once(self.dt)
                              
    def dt(self, *args):
        self.output_text = self.buffer
        
    def copy_to_clipboard(self):
        try:
            Clipboard.put(self.output_text.decode('utf-8'))
        except UnicodeError:
            out = self.output_text[:]
            out = out.translate({8216:u"'", 8217:u"'", 8220:u'"', 8221:u'"'})
            Clipboard.put(out.encode('utf-8', 'xmlcharrefreplace'))
        

        
class VoteTallyApp(App):
    def build(self):
        ExceptionManager.add_handler(E())
        Config.set('input', 'mouse', 'mouse,disable_multitouch')
        storage = self.user_data_dir
        global main_widget
        main_widget = FirstWidget(storage)
        return main_widget
        
    def on_pause(self):
        return True

    def on_resume(self):
        pass

    
if __name__ == "__main__":
    VoteTallyApp().run()
