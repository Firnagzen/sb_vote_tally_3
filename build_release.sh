VERSION=`egrep "__version__ =" main.py | cut -d \' -f 2`
sudo rm -rf bin/*
sudo buildozer android release
sudo jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/keystores/votetally.keystore "bin/SBVoteTally-"$VERSION"-release-unsigned.apk" cb-play
sudo ~/.buildozer/android/platform/android-sdk-21/tools/zipalign -v 4 "bin/SBVoteTally-"$VERSION"-release-unsigned.apk" "bin/SBVoteTally-"$VERSION".apk"
sudo ~/.buildozer/android/platform/android-sdk-21/platform-tools/adb install -r "bin/SBVoteTally-"$VERSION".apk"
sudo ~/.buildozer/android/platform/android-sdk-21/platform-tools/adb push "bin/SBVoteTally-"$VERSION".apk" /sdcard/SoftwareSandbox
